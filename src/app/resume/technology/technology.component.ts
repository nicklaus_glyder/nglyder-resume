import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.scss']
})
export class TechnologyComponent implements OnInit {
  techIcons: {}[];

  constructor() { }

  ngOnInit() {
    this.techIcons = [
      {src: 'amazonwebservices',  name: 'AWS'},
      {src: 'angularjs',      name: 'Angular'},
      {src: 'bitbucket',      name: 'Bitbucket'},
      {src: 'bootstrap' ,     name: 'Bootstrap'},
      {src: 'c' ,             name: 'C'},
      {src: 'cplusplus' ,     name: 'C++'},
      {src: 'coffeescript',   name: 'CoffeeScript'},
      {src: 'd3js',           name: 'D3'},
      {src: 'git',            name: 'Git'},
      {src: 'postgresql',     name: 'Hadoop'},
      {src: 'java',           name: 'Java'},
      {src: 'javascript',     name: 'JavaScript'},
      {src: 'mongodb',          name: 'MongoDB'},
      {src: 'linux',          name: '*NIX'},
      {src: 'nodejs',         name: 'NodeJS'},
      {src: 'python',         name: 'Python'},
      {src: 'rails',          name: 'Ruby on Rails'},
      {src: 'ruby',           name: 'Ruby'},
      {src: 'sass',           name: 'SASS'},
      {src: 'apache',         name: "Spark"},
      {src: 'typescript',     name: 'TypeScript'}
    ];
  }

}
