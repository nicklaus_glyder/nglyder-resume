import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { ResumeRoutingModule } from './resume-routing.module';
import { ResumeComponent } from './resume.component';
import { ProfileComponent } from './profile/profile.component';
import { AwardsComponent } from './awards/awards.component';
import { EducationComponent } from './education/education.component';
import { TechnologyComponent } from './technology/technology.component';
import { WorkComponent } from './work/work.component';
import { PersonalComponent } from './personal/personal.component';
import { TechIconComponent } from './technology/tech-icon/tech-icon.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ResumeRoutingModule
  ],
  declarations: [
    ResumeComponent,
    ProfileComponent,
    AwardsComponent,
    EducationComponent,
    TechnologyComponent,
    WorkComponent,
    PersonalComponent,
    TechIconComponent
  ]
})
export class ResumeModule { }
