import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResumeComponent } from  './resume.component';
import { ProfileComponent } from './profile/profile.component';
import { EducationComponent } from './education/education.component';
import { WorkComponent } from './work/work.component';
import { TechnologyComponent } from './technology/technology.component';
import { AwardsComponent } from './awards/awards.component';
import { PersonalComponent } from './personal/personal.component';

const routes: Routes = [
  {
    path: 'resume',
    component: ResumeComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: "/resume/(resume-content:profile)" },
      { path: 'profile', component: ProfileComponent, outlet: 'resume-content' },
      { path: 'education', component: EducationComponent, outlet: 'resume-content' },
      { path: 'work', component: WorkComponent, outlet: 'resume-content' },
      { path: 'technology', component: TechnologyComponent, outlet: 'resume-content' },
      { path: 'awards', component: AwardsComponent, outlet: 'resume-content' },
      { path: 'personal', component: PersonalComponent, outlet: 'resume-content' },
      { path: '**', component: ProfileComponent, outlet: 'resume-content', pathMatch: 'prefix'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResumeRoutingModule { }
