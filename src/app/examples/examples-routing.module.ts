import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExamplesComponent } from './examples.component';

const routes: Routes = [
  {
    path: 'examples',
    component: ExamplesComponent,
    children: [
      { path: '**', pathMatch: 'prefix', redirectTo: '/examples'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamplesRoutingModule { }
